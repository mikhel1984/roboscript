# Test elements of the RoboScript language
from RoboScript.rsinterp import RS_Code, RS_Directive

class RS(RS_Code):

  def move(self,x):
    self.buf += ['move({})'.format(str(x))] 

parse = RS("Test") 
parse.directiveAction(RS_Directive('test')) 

# 'print' function
txt = 'print("Hello World")'
parse.fromStr(txt) 

# arithmetic
txt = '''
a = 1+2*3 
print("Expected 7")
print(a)'''
parse.fromStr(txt) 

# in one line
txt = 'a = (1+2)*3 print("Expected 9") print(a)'
parse.fromStr(txt)

# more complex equation
txt = '''
a = (1+2)*(3-4) 
print("Expected -3")
print(a)'''
parse.fromStr(txt) 

# variable is not defined
txt = 'print("Expected NULL") print(a)'
# TODO: should be NULL 
parse.fromStr(txt) 

# concatenation
txt = 'print("Hello " + "World!")'
parse.fromStr(txt)

# conversation
txt = '''
print(int(42))
print(real(42))
print(str(42))
'''
parse.fromStr(txt)

# single character
txt = '''
a = "abc"
print("Expected 'b'")
print(a[1])'''
parse.fromStr(txt)

# logical equation
txt = '''
a = true and false or (not true) or true
print("Expected true")
print(a)'''
parse.fromStr(txt) 

# comparison
txt = 'print("Expected false") print(2*2 > 1+3)'
parse.fromStr(txt) 

# negative values
txt = 'a = -1 b = -2 print("Expected 0") print(a*4 - b*2)'
parse.fromStr(txt) 

# lists
txt = '''
a = [1,2,3]
print("Expected 3")
print(a[2])'''
parse.fromStr(txt) 

# list with coma in the end
txt = '''
a = [1,2,3,]
print("Expected 3")
print(a[2])'''
parse.fromStr(txt) 

# list length
txt = '''
a = [1,2,3]
print("Expected 3")
print(len(a))'''
parse.fromStr(txt) 

# dictionary
txt = '''
a = {p: 1, q: 2, l: 3}
print("Expected 2")
print(a.q)
print("Expected 3")
print(a["l"])'''
parse.fromStr(txt) 

# dictionary with coma in the end
txt = '''
a = {p: 1, q: 2, l: 3,}
print("Expected 2")
print(a.q)'''
parse.fromStr(txt) 

# new dictionary elements
txt = '''
a = {x:1}
a.y = 2
a["z"] = 3
print("Expected {x:1,y:2,z:3}")
print(a)'''
parse.fromStr(txt)

# dictionary keys
txt = '''
a = {p:1, q:2, r:3}
print("Expected ['p','q','r']")
print(keys(a))'''
parse.fromStr(txt) 

# complex combination
txt = '''
a = [1,{p:2, q:[3,4]},5]
print("Expected 4")
print(a[1].q[1])''' 
parse.fromStr(txt) 

# set to dictionary
xt = '''
b = {a:0,b:1,c:2}
b.c = b.a
print("Expected {a:0,b:1,c:0}")
print(b)'''
parse.fromStr(txt) 

# set to list
txt = '''
a = [10.1,20.2,30.3]
a[1] = a[1] - a[0]
print("Expected [10.1,10.1,30.3]")
print(a)'''
parse.fromStr(txt) 

# list operations (elementwise)
txt = '''
a = [[1,2],[3,4]] + [[5,6],[7,8]]
print("Expected [[6,8],[10,12]]")
print(a)
b = [1,2,3] * 3
print("Expected [3,6,9]")
print(b)'''
parse.fromStr(txt)

# function definition and call
txt = '''
def fn(x,y) return x + y end
print("Expected 6.0")
print(fn(2.5,3.5))'''
parse.fromStr(txt) 

# create alias
txt = '''
def fn(x,y) 
  a = 2*x
  b = 3*y
  return a + b 
end
fn1 = fn
print("Expected 47")
print(fn1(10,9))''' 
parse.fromStr(txt) 

# function as argument
# keywords are case-insensitive
txt = '''
def fn1(x) return 2*x end
DEF fn2(f,x) return f(x) END
print("Expected 4")
print(fn2(fn1,2))'''
parse.fromStr(txt) 

# global variable
txt = '''
a = 10
def fn(x) return a * x end
print("Expected 20")
print(fn(2))'''
parse.fromStr(txt) 

# args by keys
txt = '''
def fn(x,y,z) return x + y + z end
print("Expected 6")
print(fn(y:2,x:3,z:1))'''
parse.fromStr(txt) 

# default values
txt = '''
def fn(x:1,y:2,z:3) return x + y + z end
print("Expected 6")
print(fn())'''
parse.fromStr(txt) 

# part of arguments
txt = '''
def fn(x:1,y:2,z:3) return x + y + z end
print("Expected 9")
print(fn(4))'''
parse.fromStr(txt) 

# mix of position and name
txt = '''
def fn(x:1,y:2,z:3) return x + y + z end
print("Expected 10")
print(fn(4,z:4))'''
parse.fromStr(txt) 

# expand dictionary
txt = '''
d = {y:3,z:2}
def fn(x:1,y:2,z:3) return x + 2*y + 3*z end
print("Expected 13")
print(fn(*d,1))'''
parse.fromStr(txt) 

# expand list
txt = '''
d = [3,4]
def fn(x:1,y:2,z:3) return x + 2*y + 3*z end
print("Expected 20")
print(fn(2,*d))'''
parse.fromStr(txt) 

# change elements in global containers
txt = '''
a = [1,2,3]
def fn(x) x[0] = 0 end
fn(a)
print("Expected [0,2,3]")
print(a)'''
parse.fromStr(txt) 

# get function arguments
txt = '''
def fn(x:1,y:2,z:3) 
  d = args()
  print(d)
end
print("Expected {x:10,y:20,z:30}")
fn(10,20,30)
'''
parse.fromStr(txt) 

# conditions
txt = '''
print("Expected '1 < 2'") 
if 1 > 2 then     print("1 > 2")
elseif 1 < 2 then print("1 < 2") 
else              print("1 == 2") 
end '''
parse.fromStr(txt)

# use condition in loop
txt = '''
i = 0 j = 0
while true do
  i = i + 1
  if i > 5 then break
  elseif i > 2 then continue
  end
  j = j + 1
end 
print("Expected 2")
print(j)
'''
parse.fromStr(txt)

# use return 
txt = '''
def abs(x) 
  if x < 0 then return -x else return x end
end 
print("Expected 3")
print(abs(-3))
'''
parse.fromStr(txt)

# directives
txt = '''
@ all
"Try directive"
print("Expected: 'Try directive'")
'''
parse.fromStr(txt)
parse.disp()

# import other module
txt = '''
m = import("module1")
print("Expected 3 and 4")
print(m.sum(1,2))
-- module name is also available 
print(module1.sum(2,2))
'''
parse.fromStr(txt)

print("Done!")