# Lexical elements of the RoboScript

from ply import *

######################################### Tokens #############################

tokens = (
  'NAME',
  'STRING',
  'NUMBER',
  # signs
  'COLON',
  'SEMICOLON',
  'COMA',
  'DOT',   
  'LPAR',
  'RPAR',
  'LSQR',
  'RSQR',
  'LBRC',
  'RBRC',
  # operations
  'SET',
  'PLUS',
  'MINUS',
  'MULT',
  'DIV',  
  'LT',
  'LE',
  'GT',
  'GE',
  'EQ',
  'NE',
  'AND',
  'OR',
  'NOT',
  # keywords   
  'END',
  'DEF',
  'RETURN',  
  'IF',
  'THEN',
  'ELSEIF',
  'ELSE',
  'WHILE',
  'DO',
  'DOG',  
  # reserved   
  'NULL',
  'BOOL',
  'BREAK',
)

t_COLON = r':'
t_SEMICOLON = r';'
t_COMA = r','
t_DOT = r'\.' 
t_LPAR = r'\('
t_RPAR = r'\)'
t_LSQR = r'\['
t_RSQR = r'\]'
t_LBRC = r'\{'
t_RBRC = r'\}'
t_DOG = r'@'

t_SET = r'='
t_PLUS = r'\+'
t_MINUS = r'-'
t_MULT = r'\*'
t_DIV = r'/'

t_LT = r'<'
t_LE = r'<='
t_GT = r'>'
t_GE = r'>='
t_EQ = r'=='
t_NE = r'<>' 

RESERVED = {
  "end" : "END",
  "def" : "DEF", "return" : "RETURN",
  "if" : "IF", "then" : "THEN", "elseif" : "ELSEIF", "else" : "ELSE", 
  "while" : "WHILE", "do" : "DO", 
  "null" : "NULL",
  "true" : "BOOL", "false" : "BOOL",
  "break" : "BREAK", "continue" : "BREAK",
  "and" : "AND", "or" : "OR", "not" : "NOT",
}

t_ignore = ' \t' 

def t_NAME(t):
  r'[a-zA-Z_][a-zA-Z0-9_]*'
  t.type = RESERVED.get(t.value.lower(), "NAME")
  return t
 
def t_NUMBER(t): 
  r"\d+(\.\d+)?([eE][-+]?\d+)?"
  val = t.value 
  if val.find('.') > -1 or val.find('e') > -1 or val.find('E') > -1:
    t.value = float(val)
  else:
    t.value = int(val)
  return t
  
def t_STRING(t):
  r'"([^\\\"]+|\\\")*"'
  t.value = t.value[1:-1]
  return t 
  
def t_comment(t):
  r"[ ]*--[^\n]*"
  pass 
   
def t_newline(t):
  r"\n+"
  t.lexer.lineno += len(t.value)  
  
def t_error(t):
  print("Illegal character '%s'" % t.value[0]) 
  t.lexer.skip(1) 
  

lexer = lex.lex()  
   
