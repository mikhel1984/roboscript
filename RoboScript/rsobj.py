
import sys 
import re


class RS_Element:
  "Base class for the language elements of RobotScript" 
  
  def __init__(self,type):
    self.type = type  
      
  def __eq__(self,other):
    return self.val == other 
    
  def __ne__(self,other):
    return self.val != other 
    
  def type(self):
    return self.type 
  
  def exit(self,name):
    sys.exit("{} is not defined for {}".format(name,self.type))  
    
  def wrong_type(self,val):
    self.wrong_type(self.type,val)
        
  def wrong_type(self,tp,val):
    sys.exit("{} is not of the type {}".format(val,tp))
 


class RS_Number(RS_Element):
  "Number container"
  
  def __init__(self,val):
    super().__init__("Number")
    self.val = val 
    
  def get(self):
    return self.val 
    
  def __str__(self):
    return str(self.val)
        
  def eval(self,op,rhs):
    if op == '+':
      return RS_Number(self.get() + rhs.get())
    elif op == '-':
      return RS_Number(self.get() - rhs.get())
    elif op == '*':
      if isinstance(rhs,RS_List):
        return rhs.eval(op,self)
      return RS_Number(self.get() * rhs.get())
    elif op == '/':
      return RS_Number(self.get() / rhs.get())
    elif op == 'MINUS':
      return RS_Number(-self.get())
    elif op == '>':
      return RS_Bool(self.get() > rhs.get())
    elif op == '>=':
      return RS_Bool(self.get() >= rhs.get())
    elif op == '<':
      return RS_Bool(self.get() < rhs.get())
    elif op == '<=':
      return RS_Bool(self.get() <= rhs.get())
    elif op == '==':
      return RS_Bool(self.get() == rhs.get())
    elif op == '<>':
      return RS_Bool(self.get() != rhs.get())
    else:
      self.exit(op)
      
  
class RS_String(RS_Element):
  "String container"
  
  def __init__(self,val): 
    super().__init__("String")
    self.val = val
  
  def __str__(self):
    return '"{}"'.format(self.val,)
  
  def get(self,ind=None):
    if ind:
      # get character
      if isinstance(ind,RS_Number): ind = ind.get() 
      return RS_String(self.val[ind])
    else:
      # get full string
      return self.val 
    
  def eval(self,op,rhs):
    if op == '+':
      return RS_String(self.get() + rhs.get())
    else:
      self.exit(op) 
      
  def len(self):
    return len(self.val) 

class RS_Bool(RS_Element):
  "Boolean value container"
  
  def __init__(self,val):
    super().__init__("Bool")
    if type(val) is bool:
      self.val = val
    elif val.lower() == 'true':
      self.val = True
    elif val.lower() == 'false':
      self.val = False 
    else:
      self.wrong_type(val) 
      
  def get(self):
    return self.val 
    
  def __str__(self):
    return 'true' if self.val else 'false' 
  
  def eval(self,op,rhs):
    if op == 'and':
      return RS_Bool(self.get() and rhs.get())
    elif op == 'or':
      return RS_Bool(self.get() or rhs.get())
    elif op == 'NOT':
      return RS_Bool(not self.get())
    else:
      self.exit(op)
    
class RS_Null(RS_Element):
  "Element without value"
  
  def __init__(self):
    super().__init__("NULL")
    self.val = None
      
  def get(self):
    return None 
    
  def __str__(self):
    return 'NULL'
    

class RS_Dict(RS_Element):
  "Dictionary container"
  
  def __init__(self):
    super().__init__("Dict") 
    self.dict = {}      # current values
    self.block = {}     # don't look in parents 
    self.parents = []   # 'inheritance'  
    
  def __str__(self):
    v = ["{}:{}".format(x[0],str(x[1])) for x in self.dict.items()]
    return "{}{}{}".format('{',','.join(v),'}')
      
  def set(self,key,val):
    if type(key) is RS_String:
      s = key.get()
      found = re.search(r'[a-zA-Z_][a-zA-Z0-9_]*',s)
      if s == found.group():
        key = s
    if type(key) is str:  # key is a proper name 
      if val:
        self.dict[key] = val      
      else:
        # remove element if the value is None 
        self.dict.pop(key,None)   # remove from the main dictionary
        self.block[key] = True    # skip in parent elements 
    else:
      sys.exit("{} is not a proper name".format(key,))
      
  def parent(self,par):
    if not par:
      pass 
    if type(par) is RS_Dict:
      self.parents.append(par)
    else:
      self.wrong_type(par)      
      
  def get(self,key):
    if isinstance(key,RS_String): key = key.get() 
    res = self.dict.get(key) 
    if not (res or self.block.get(key)):
      for v in self.parents:
        res = v.get(key)
        if res: break 
    return res 
      
  def insert(self,obj):
    if type(obj) is not RS_Dict:
      self.wrong_type(obj)
    self.dict.update(obj.dict) 
  
  def keys(self):
    return list(self.dict.keys()) 

  def keylist(self):
    lst = RS_List()
    for k in self.dict.keys():
      lst.append(RS_String(k))
    return lst
    
  def getParent(self,ind=0):
    return self.parents[ind] 
        
    
class RS_List(RS_Element):
  "List container"
  
  def __init__(self):
    super().__init__("List")
    self.list = []
    
  def __str__(self):
    return "[{}]".format(','.join([str(x) for x in self.list],)) 

  def set(self,ind,val):
    n = len(self.list) 
    if isinstance(ind,RS_Number): ind = ind.get()     
    if ind == n:
      self.list.append(val)
    else:
      self.list[ind] = val 
      
  def append(self,val):
    self.list.append(val) 
    
  def get(self,ind):
    if isinstance(ind,RS_Number): ind = ind.get() 
    return self.list[ind]  
     
  def insert(self,obj):
    if type(obj) is not RS_List:
      self.wrong_type(obj)
    self.list += obj.list 

  def items(self):
    return self.list
    
  def len(self):
    return len(self.list)
    
  def eval(self,op,rhs):
    res = RS_List()
    if op == '+': 
      # elementwise sum        
      for i,v in enumerate(self.list):
        res.append(v.eval(op,rhs.get(i))) 
    elif op == '-':
      # elementwise difference
      for i,v in enumerate(self.list):
        res.append(v.eval(op,rhs.get(i))) 
    elif op == '*':
      # multiply with variable
      for v in self.list:
        res.append(v.eval(op,rhs)) 
    elif op == '/':
      # divide on variable
      for v in self.list:
        res.append(v.eval(op,rhs)) 
    else:
      self.exit(op)
    return res

