# Syntax rules of the RoboScript

# TODO: add language elements
# * simplified function call 

from ply import * 
import RoboScript.rslex as rslex
from RoboScript.rsobj import *
import RoboScript.keywords as key

# tree elements 
# (SET_, variable, expression) - define variable name
# (IND_, list, index) - access to list element
# (KEY_, dictionary, name) - access to dictionary element
# (LIST_, elements) - create list with elements
# (DICT_, elements) - create dictionary with elements 
# (EXPAND_, element) - insert elements of list / dictionary 
# (PARENT_, element) - set parent dictionary
# (DEF_, name, arguments, body)
# (VAR_, name) - use variable name
# (EVAL_, name, arguments) - evaluate function with the given arguments
# (KEYVAL_, name, value) - set value to key
# (DIRECTIVE, name, object) - use object with the given directive
# (WHILE, expression, block) - while loop
# (IF, ifendif, else) - if - else condition


tokens = rslex.tokens 

def p_program2(p):
  '''program : program codeblock''' 
  p[0] = p[1] + [p[2]]  
      
def p_program0(p):
  '''program : ''' 
  p[0] = [] 

def p_error(p):
  if p:
    print("Syntax error at token", p.type, p.lineno)
    # Just discard the token and tell the parser it's okay.
    parser.errok()
  else:
    print("Syntax error at EOF")
  
def p_codeblock(p):
  '''codeblock : block
               | directive
               | funcdef'''  
  p[0] = p[1] 
  
def p_block2(p):
  '''block : block state'''  
  p[0] = p[1] + [p[2]]
      
def p_block1(p):
  '''block : state'''  
  p[0] = [p[1]]   
    
def p_state3(p):
  '''state : var SET expr'''   
  p[0] = (key.P_SET,p[1],p[3]) 
  
      
def p_state1(p):
  '''state : funcall'''   
  p[0] = p[1]  
  
def p_retstate1(p):
  '''state : RETURN expr'''
  p[0] = (key.P_RETURN,p[2]) 
    
#def p_state2(p):
#  '''state : simpcall''' 
#  p[0] = p[1] 

# loop
    
def p_state5(p):
  '''state : WHILE expr DO whileblk END'''   
  p[0] = (key.P_WHILE,p[2],p[4])  
  
def p_whileblk2(p):
  '''whileblk : whileblk BREAK
              | whileblk state'''
  p[0] = p[1] + [p[2]]
  
def p_whileblk1(p):
  '''whileblk : '''
  p[0] = []

# condition 

def p_state6(p):
  '''state : ifblk elseblk END'''
  p[0] = (key.P_IF,p[1],p[2]) 
  
def p_ifblk1(p):
  '''ifblk : IF expr THEN thenblk'''
  p[0] = [(p[2],p[4])] 
  
def p_ifblk2(p):
  '''ifblk : ifblk ELSEIF expr THEN thenblk'''
  p[0] = p[1] + [(p[3],p[5])] 
  
def p_thenblk1(p):
  '''thenblk : thenblk state
             | thenblk BREAK'''
  p[0] = p[1] + [p[2]] 
  
def p_thenblk2(p):
  '''thenblk : '''
  p[0] = [] 
  
def p_elseblk1(p):
  '''elseblk : ELSE thenblk'''
  p[0] = p[2]
  
def p_elseblk2(p):
  '''elseblk : '''
  p[0] = [] 
  
# expression / variable

def p_var4(p):
  '''var : var LSQR expr RSQR'''
  p[0] = (key.P_IND,p[1],p[3])    
    
def p_var3(p):
  '''var : var DOT NAME'''
  p[0] = (key.P_KEY,p[1],p[3])  

def p_var1(p):
  '''var : NAME'''
  p[0] = (key.P_VAR,p[1]) 
  
def p_expr1(p):
  '''expr : compor          
          | dict'''     
  p[0] = p[1] 

def p_expr_null(p):
  '''expr : NULL'''
  p[0] = RS_Null() 
    
def p_expr3(p):
  '''expr : LPAR expr RPAR'''     
  p[0] = p[2]
      
def p_computable(p):
  '''computable : var
                | funcall
                | list''' 
  p[0] = p[1]
  
def p_computable_num(p):
  '''computable : NUMBER'''
  p[0] = RS_Number(p[1])
  
def p_computable_str(p):
  '''computable : STRING'''
  p[0] = RS_String(p[1])
  
def p_computable_bool(p):
  '''computable : BOOL'''
  p[0] = RS_Bool(p[1])
          

# Lists
  
def p_list(p):
  '''list : LSQR listline RSQR'''
  p[0] = (key.P_LIST,p[2])
  
def p_list1(p):
  '''list : LSQR listline COMA RSQR'''
  p[0] = (key.P_LIST,p[2])
  
def p_listline3(p):
  '''listline : listline COMA listelt'''
  p[0] = p[1] + [p[3]]  
    
def p_listline1(p):
  '''listline : listelt'''
  p[0] = [p[1]]
  
def p_listline0(p):
  '''listline : '''
  p[0] = None 
    
def p_listelt1(p):
  '''listelt : expr''' 
  p[0] = p[1]
    
def p_listelt2(p):
  '''listelt : MULT var''' 
  p[0] = (key.P_EXPAND,p[2])
      
# Dictionaries 

def p_dict(p):
  '''dict : LBRC dictline RBRC'''
  p[0] = (key.P_DICT,p[2]) 
  
def p_dict1(p):
  '''dict : LBRC dictline COMA RBRC'''
  p[0] = (key.P_DICT,p[2])

def p_dictline3(p):
  '''dictline : dictline COMA dictelt'''
  p[0] = p[1] + [p[3]]  
    
def p_dictline1(p):
  '''dictline : dictelt'''
  p[0] = [p[1]]
  
def p_dictline0(p):
  '''dictline : '''
  p[0] = None 
  
def p_keyval(p):
  '''keyval : NAME COLON expr'''
  p[0] = (key.P_KEYVAL,p[1],p[3])
    
def p_dictelt3(p):
  '''dictelt : keyval'''
  p[0] = p[1]
      
def p_dictelt2(p):
  '''dictelt : MULT var'''              
  p[0] = (key.P_EXPAND,p[2])   

def p_dictelt1(p):
  '''dictelt : var'''              
  p[0] = (key.P_PARENT,p[1]) 
    
# Arithmetic / Logic operations 

def p_compor3(p):
  '''compor : compor OR compand'''
  p[0] = (p[2],p[1],p[3])
      
def p_compor1(p):
  '''compor : compand'''
  p[0] = p[1] 

def p_compor2(p):
  '''compor : NOT compand'''
  p[0] = (key.P_NOT,p[2])  

def p_compand3(p):
  '''compand : compand AND bincomp'''
  p[0] = (p[2],p[1],p[3])
  
def p_compand1(p):
  '''compand : bincomp'''
  p[0] = p[1]

def p_bincomp3(p):
  '''bincomp : binexpr GT binexpr 
             | binexpr GE binexpr
             | binexpr LT binexpr
             | binexpr LE binexpr
             | binexpr EQ binexpr
             | binexpr NE binexpr''' 
  p[0] = (p[2],p[1],p[3])
      
def p_bincomp1(p):
  '''bincomp : binexpr''' 
  p[0] = p[1]

def p_binexpr3(p):
  '''binexpr : binexpr PLUS binterm
             | binexpr MINUS binterm''' 
  p[0] = (p[2],p[1],p[3])
      
def p_binexpr2(p):
  '''binexpr : MINUS binterm''' 
  p[0] = (key.P_MINUS,p[2])
      
def p_binexpr1(p):
  '''binexpr : binterm''' 
  p[0] = p[1] 
    
def p_binterm3(p):
  '''binterm : binterm MULT binfactor
             | binterm DIV binfactor'''
  p[0] = (p[2],p[1],p[3])
      
def p_binterm1(p):
  '''binterm : binfactor'''
  p[0] = p[1] 

def p_binfactor3(p):
  '''binfactor : LPAR compor RPAR''' 
  p[0] = p[2]
  
def p_binfactor1(p):
  '''binfactor : computable''' 
  p[0] = p[1] 
    
# Funciton call  
  
def p_funcall(p):
  '''funcall : var LPAR arglst RPAR'''
  p[0] = (key.P_EVAL,p[1],p[3])

def p_arglst3(p):
  '''arglst : arglst COMA argelt''' 
  p[0] = p[1] + [p[3]]   
    
def p_arglst1(p):
  '''arglst : argelt''' 
  p[0] = [p[1]]
  
def p_arglst0(p):
  '''arglst : ''' 
  p[0] = None # []?  
    
def p_argelt3(p):
  '''argelt : keyval'''
  p[0] = p[1]
  
def p_argelt2(p):
  '''argelt : MULT var'''
  p[0] = (key.P_EXPAND,p[2])
  
def p_argelt1(p):
  '''argelt : expr'''
  p[0] = p[1]

#def p_simpcall(p):
#  '''simpcall : var arglst SEMICOLON'''
#  p[0] = ('EVAL',p[1],p[2])  

# Funciton definition 

def p_funcdef(p):
  '''funcdef : DEF NAME LPAR namelst RPAR funcblock END'''
  p[0] = (key.P_DEF,p[2],p[4],p[6])  
  
def p_namelst3(p):
  '''namelst : namelst COMA nameelt'''
  p[0] = p[1] + [p[3]]
    
def p_namelst1(p):
  '''namelst : nameelt'''
  p[0] = [p[1]]
    
def p_namelst0(p):
  '''namelst : '''
  p[0] = None      

def p_nameelt3(p):
  '''nameelt : keyval'''
  p[0] = p[1]
    
def p_nameelt1(p):
  '''nameelt : NAME''' 
  p[0] = p[1] 
   
def p_funcblock1(p):
  '''funcblock : block''' 
  p[0] = p[1]
  
def p_funcblock0(p):
  '''funcblock : ''' 
  p[0] = None    

# Translator directive

def p_directive1(p):
  '''directive : DOG NAME funcdef
               | DOG NAME state''' 
  p[0] = [(key.P_DIRECTIVE,p[2],p[3])]  
  
def p_directive2(p):
  '''directive : DOG NAME STRING'''
  p[0] = [(key.P_DIRECTIVE,p[2],RS_String(p[3]))]
    
parser = yacc.yacc()

  
