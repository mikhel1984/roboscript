
# parser elements

# actions 
P_SET = 'SET'
#WHILE_ = 'WHILE'
P_IND = 'IND'
P_KEY = 'KEY'
P_LIST = 'LIST'
P_DICT = 'DICT'
P_EXPAND = 'EXPAND'
P_PARENT = 'PARENT'
P_KEYVAL = 'KEYVAL'
P_DEF = 'DEF'
P_DIRECTIVE = 'DIRECTIVE'
P_VAR = 'VAR'
P_EVAL = 'EVAL'
P_RETURN = 'RETURN'
P_WHILE = 'WHILE'
P_IF = 'IF'

P_NOT = 'NOT'
P_MINUS = 'MINUS'

# reserved
R_BREAK = 'break'
R_CONTINUE = 'continue'

# trajectory
D_POINTS = 'points'

# move 
D_POINT  = 'point'
D_TYPE   = 'type'
D_LIN    = 'lin'
D_PTP    = 'ptp'
D_CIRC   = 'circ'
D_PROC   = 'proc'
D_IN     = 'in'
D_VEL    = 'vel'
D_ACC    = 'acc'
D_SMOOTH = 'smooth'

# configuration
D_PIECE  = 'piece'
D_BASE   = 'base'
D_TOOL   = 'tool'
