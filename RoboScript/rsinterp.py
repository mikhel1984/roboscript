# Basic interpretator elements
import sys 

import RoboScript.rsyacc as rsyacc
from RoboScript.rsobj import *
import RoboScript.keywords as keywords


RS_op_bin = ('+','-','*','/','and','or','>','>=','<','<=','==','<>')
RS_op_un = (keywords.P_NOT,keywords.P_MINUS)
RS_STOP = (keywords.R_BREAK,keywords.R_CONTINUE)


class RS_Func(RS_Element):
  "Function container"
  
  def __init__(self,argkeys=None,argvals=None,code=None,fn=None):
    super().__init__("Function")
    if code:
      self.code = code
      self.fn = None
    elif fn:
      self.fn = fn
      self.code = None
    else:
      sys.exit("Function is undefined")    
    self.keys = argkeys     # list of the argument names
    self.default = argvals  # list of the argument values
        
  def __str__(self):
    return "Function({})".format(','.join(self.keys),) 
        
  def eval(self,env_,args=None):
    env = self.args_2_dict(args,env_) 
    result = None
    if self.fn:
      result = self.fn(env)
    elif self.code:
      for stat in self.code:
        result = RS_eval(stat,env)
    else:
      sys.exit("Function is not defined!") 
    return result
      
  def args_2_dict(self,args,env):
    d = RS_Dict()
    i = 0
    lst = []
    if args: 
      # prepare arguments
      for arg in args:
        if type(arg) is tuple:
          if arg[0] == keywords.P_KEYVAL:
            # key : value
            d.set(arg[1],arg[2]) 
          elif arg[0] == keywords.P_EXPAND:
            val = RS_simp(arg[1],env)
            if type(val) is RS_Dict:
              # add dictionary
              d.insert(val)
            else:
              # add list
              lst += val.items()
          else:
            # add the rest to the list
            lst.append(arg)
        else:
          lst.append(arg)
    # add defaults if need
    n = len(lst)
    if self.default and n < len(self.default):
      lst += self.default[n:]
    for i,v in enumerate(lst):
      key = self.keys[i] 
      if d.get(key):
        # error for non-default values
        if i < n: sys.exit("Double argument definition")
      else:
        d.set(key,v) 
    # recalculate for the all keys
    for k in d.keys():
      v = d.get(k)      
      d.set(k, RS_simp(v,env))
    # inheritance 
    d.parent(env)
    return d
    
class ReturnExcept(Exception):
  def __init__(self,value):
    self.value = value 
    
def RS_simp(expr,env):
  val = RS_eval(expr,env)
  if type(val) is tuple:
    val = val[0].get(val[1])
  return val

def RS_eval(expr,env):
  if isinstance(expr,RS_Element):
    return expr
  if type(expr) is tuple:
    cmd = expr[0]
    if cmd == keywords.P_SET:
      # assign value to variable or list/dict
      obj,ind = RS_eval(expr[1],env)  # return object link and key/index or environment and variable name 
      val = RS_simp(expr[2],env)
      obj.set(ind,val)
    elif cmd == keywords.P_IND:      
      # get list index 
      obj, key = RS_eval(expr[1],env)     
      ind = RS_eval(expr[2],env)
      return obj.get(key),ind
    elif cmd == keywords.P_KEY:
      # get dictionary and key
      obj, key = RS_eval(expr[1],env) 
      ind = expr[2]
      return obj.get(key),ind
    elif cmd == keywords.P_LIST:
      # make list
      lst = RS_List()
      # add 'expand' 
      for k in expr[1]:
        val = RS_eval(k,env)
        lst.append(val)
      return lst
    elif cmd == keywords.P_DICT:
      # make dictionary
      dct = RS_Dict()
      for k in expr[1]:        
        if k[0] == keywords.P_KEYVAL:
          # single value
          val = RS_simp(k[2],env)
          dct.set(k[1],val)
        elif k[0] == keywords.P_PARENT:
          # inheritance
          val = RS_simp(k[1],env)
          dct.parent(val)
        elif k[0] == keywords.P_EXPAND:
          val = RS_simp(k[1],env) 
          dct.insert(val)          
      return dct   
    elif cmd == keywords.P_VAR:
      # get variable
      return env, expr[1]
    elif cmd == keywords.P_EVAL:
      # function call 
      fn = RS_simp(expr[1],env)
      v = None
      try:
        v = fn.eval(env,expr[2])
      except ReturnExcept as e:
        return e.value
      return v
    elif cmd == keywords.P_RETURN:
      # function return value
      #return RS_simp(expr[1],env)
      raise( ReturnExcept(RS_simp(expr[1],env)) )
    elif cmd in RS_op_bin:
      # binary operations
      lhs = RS_simp(expr[1],env)
      rhs = RS_simp(expr[2],env)
      return lhs.eval(cmd,rhs)
    elif cmd in RS_op_un:
      # unary operations
      lhs = RS_simp(expr[1],env)    
      return lhs.eval(cmd,None)
    elif cmd == keywords.P_WHILE:
      # while loop
      while True:
        cond = RS_simp(expr[1],env)
        if cond.get() in (False,0,None):
          break
        else:
          # do internal block
          v = None
          for st in expr[2]:
            v = RS_eval(st,env)
            if v in RS_STOP: break
          # check exit (continue statement is "automatic")
          if v == keywords.R_BREAK:
            break
    elif cmd == keywords.P_IF:
      # condition
      for elt in expr[1]:
        cond = RS_simp(elt[0],env) 
        if cond.get():
           # evaluate if true 
           for st in elt[1]:
             v = RS_eval(st,env)
             if v in RS_STOP: return v 
           break 
      else:
        # evaluate in "else" condition 
        for st in expr[2]:
          v = RS_eval(st,env) 
          if v in RS_STOP: return v         
    else:
      print("!!!",cmd)
      sys.exit("Undefined command")
  elif expr in RS_STOP:
    return expr
  else:
    print("Achtung!!!",expr)
    sys.exit("!!!") 


class RS_Code():
  "RoboScript processing interface"

  def __init__(self,model):
    self.model = model    # robot name 
    self.glob = RS_Dict() 
    self.buf = [] 
    self.directives = None
    self.q0 = None
    self.imported = [] 
    self.locals = None

    # set "standard" functions 
    # print argument
    self.glob.set('print',RS_Func(['x'],
         fn = lambda arg: print(arg.get('x'))))
    # get list length
    self.glob.set('len',RS_Func(['x'],
         fn = lambda arg: arg.get('x').len()))
    # robot motion 
    self.glob.set('move',RS_Func(['x'],
         fn = self.move))
    # get dictionary keys
    self.glob.set('keys',RS_Func(['x'],
         fn = lambda arg: arg.get('x').keylist()))
    # get function arguments
    self.glob.set('args',RS_Func([],
         fn = self._args))
    # convert to integer
    self.glob.set('int',RS_Func(['x'],
         fn = lambda arg: RS_Number(int(arg.get('x').get()) )))
    # convert to floating point
    self.glob.set('real',RS_Func(['x'],
         fn = lambda arg: RS_Number(float(arg.get('x').get()) )))
    # convert to string
    self.glob.set('str',RS_Func(['x'],
         fn = lambda arg: RS_String(str(arg.get('x')) )))
    # load module
    self.glob.set('import',RS_Func(['x'],
         fn = self._import)) 
         
  def directiveAction(self,obj):
    self.directives = obj
    
  def setInitial(self,q0):
    self.q0 = q0 

  def fromFile(self,fname):
    f = open(fname,"r")
    return self.fromStr(f.read())
    
  def fromStr(self,text):
    ast = rsyacc.parser.parse(text)
    return self.fromAST(ast)
    
  def tree(self,txt): # debug
    return rsyacc.parser.parse(txt) 

  def fromAST(self,result):
    # clear buffer
    self.buf = []
    # find functions and instructions
    fnclst = []
    blklst = []
    for elt in result:
      if elt[0] == keywords.P_DEF:
        fnclst.append(elt)
      else:
        blklst += elt
    # build environment
    GLOBAL = RS_Dict()
    GLOBAL.parent(self.glob)
    # save functions  
    for elt in fnclst:
      keys,vals = self.RS_arglist(elt[2])
      fn = RS_Func(keys,vals,code=elt[3]) 
      GLOBAL.set(elt[1],fn)  # name, func   
        
    # evaluate code
    for stat in blklst:
      if stat[0] == keywords.P_DIRECTIVE:
        if self.directives:
          self.buf += self.directives.process(stat[1],stat[2],GLOBAL) 
        else:
          print("Warning: no directives")                
      else:
        RS_eval(stat,GLOBAL) 
    # add head
    lns = self.head(GLOBAL)
    self.buf = lns + self.buf
    # add foot
    lns = self.foot(GLOBAL)
    self.buf += lns
    # save current state
    self.locals = GLOBAL
    
    # return abstract syntax tree 
    return result  

  def RS_arglist(self,elts):
    keys, vals = [], []
    for v in elts:
      if len(v) == 1:
        keys.append(v[0])
        vals.append(None)
      elif len(v) == 3:
        keys.append(v[1])
        vals.append(RS_eval(v[2],None))
      else:
        sys.exit("Wrong argument list")
    return keys, vals 
    
  def disp(self):
    print('\n'.join(self.buf)) 
    
  def save(self,fname):
    f = open(fname,'w')
    f.write('\n'.join(self.buf))
    f.close() 
    
  def _args(self,v):
    res = RS_Dict()
    res.insert(v.getParent()) 
    return res  
    
  def _import(self,nm):
    nm = nm.get('x').get() 
    if nm in self.imported:
      # do nothing
      return 
    # read and evaluate
    rs = RS_Code(nm)
    rs.fromFile(nm+'.ros')
    # save 
    self.glob.set(nm,rs.locals)
    self.imported.append(nm)   
    return rs.locals   
      
# Redefine these functions !!!
    
  def head(self,env):
    # add begining of the code
    return [] 

  def foot(self,env):
    # add end of the code
    return []

  def move(self,param):
    # define your code for the robot motion
    sys.exit("Undefined MOVE function!")

class RS_Directive():

  def __init__(self,model):
    self.model = model 
    
  def process(self,key,expr,env):
    #if type(expr) is tuple:
    if type(expr) is RS_String:
      return self.procString(key,expr,env) 
    else:
      print("Warning: undefined type of the directive") 
    
  def procString(self,cmd,val,env):
    if cmd == self.model or cmd == 'all':
      return [val.get()] 
    else:
      print("Warning: directive {} not found".format(cmd)) 
    
    
  
    
  
