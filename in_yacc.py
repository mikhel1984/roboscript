
import sys

fname = sys.argv[1]
f = open(fname,'r')
s = f.read()
f.close()

from RoboScript.rsyacc import parser 

result = parser.parse(s)
for blk in result:
  print(blk)