
import sys

fname = sys.argv[1]
f = open(fname,'r')
s = f.read()
f.close()

from RoboScript.rslex import lexer 

lexer.input(s)
for tok in lexer:
  print(tok)