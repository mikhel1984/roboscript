
import sys
from RoboScript.rsinterp import RS_Code, RS_Directive

class RS(RS_Code):

  def move(self,x):
    self.buf += ['move({})'.format(str(x))] 



parse = RS("Test") 
parse.directiveAction(RS_Directive('test')) 

parse.fromFile(sys.argv[1])